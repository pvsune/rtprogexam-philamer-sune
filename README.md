# Schelling Model (RTPROGEXAM)
Rising Tide Take-Home Exam

More details [here](http://brianbaquiran.com/stories/take-home-programming-exam/)

# Synopsis
```perl
#!/usr/bin/perl
use strict;
use warnings;

require Schelling;
my $grid = Schelling::loadgrid('/path/to/grid.txt');

# returns index of dissimilarity up to 5 decimal places
my $index = Schelling::dissimilarity($grid, 0, 0, 4, 4);

my $t = 0.33; # threshold
my $new_grid = Schelling::schelling($grid, $t);
```

# How-To-Run
On Linux distros, **PERL** is normally installed by default together with a tool running perl tests **prove**.
If these weren't available make sure to install them first.
Run the test using this command `prove -r t`


# TODOs
- [x] perldoc for each method
- [ ] check how the script would fair given a very big grid file
