package Schelling;
use strict;
use warnings;

=pod
Slurp grid text file to 2D array

Accept:
    file - complete file path of grid

Return:
    grid - 2D array data structure of grid file
=cut

sub loadgrid {
    my $file = shift;
    open my $info, $file or die "Could not open $file: $!";

    my @grid;
    while( <$info> )  {
        s/\n+//g; # remove newline chars
        my @line = split '', $_;
        push @grid, \@line;
    }

    close $info;

    # WANT: better subroutine for cheking invalid grid
    return -1 unless _find_grid_dimensions(\@grid);

    return \@grid;
}

=pod
Get index of dissimilarity of a subregion in the grid

Accept:
    grid - cleaned grid data structure
    row1, col1 - point A (upper-lefthand corner) of the subregion
    row2, col2 - poing B (lower-righthand corner) of the subregion

Return:
    index - the index of dissimilarity in five decimal places
=cut

sub dissimilarity {
    my ($grid, $row1, $col1, $row2, $col2) = @_;
    return -1 if $row1 > $row2 || $col1 > $col2;

    my $total = _find_agent_count($grid);
    my $sub = _find_agent_count($grid, $row1, $col1, $row2, $col2);

    my $X = $sub->{X} / $total->{X};
    my $O = $sub->{O} / $total->{O};
    my $index = 0.5 * abs($X - $O);

    # 5 decimal places seems precise enough
    return sprintf '%.5f', $index;
}

=pod
Creates new grid based on Schelling's Segragation Model

Accept:
    grid - cleaned grid data structure
    threshold - minimum tolerance for agent satisfaction, in decimal

Return:
    grid - new grid where disatisfied agent was replaced by vacant space
=cut

sub schelling {
    my ($grid, $t) = @_; # $grid should be clean
    return -1 if $t <= 0 || $t >= 1; # only between 0, 1

    my ($width, $length) = _find_grid_dimensions($grid);
    my @newgrid;

    for (my $x = 0; $x < $width; $x++) {
        for (my $y = 0; $y < $length; $y++) {
            my $neighbors = _find_neighbors($grid, $x, $y);

            my $agent = $grid->[$x]->[$y];
            $agent = ' ' unless _is_agent_satisfied($neighbors, $agent, $t);

            $newgrid[$x][$y] = $agent;
        }
    }

    return \@newgrid;
}

##### HELPER methods

# returns an array of width, length of the grid
sub _find_grid_dimensions {
    my $grid = shift;
    my @y = @{ $grid->[0] };

    my $x = 0;
    return if grep { $x++; scalar @$_ != scalar @y } @$grid;

    return ($x, scalar @y);
}

# tells agent satisfaction based on threshold, neighbors the agent has
# ; vacant space are not counted as neighbors
sub _is_agent_satisfied {
    my ($neighbors, $type, $t) = @_;
    return unless grep { $type eq $_ } qw/X O/;

    my %count;
    $count{$_}++ foreach @$neighbors;

    my $type_count = $count{$type} // 0; # when all neighbors are different
    return $t <= $type_count / scalar @$neighbors;
}

# returns array of agent type (X, O) an agent is surrounded in grid
sub _find_neighbors {
    my ($grid, $row, $col) = @_;
    my $startX = ($row > 0) ? $row-1 : 0;
    my $startY = ($col > 0) ? $col-1 : 0;

    my @neighbors;
    for (my $x = $startX; $x <= $row+1; $x++) {
        for (my $y = $startY; $y <= $col+1; $y++) {
            next if $x == $row && $y == $col; # skip actual point

            # care for index out-of-bounds; would not work on $arr[-1]
            my $neighbor = $grid->[$x]->[$y];
            push @neighbors, $neighbor
                if defined $neighbor && $neighbor ne ' ';
        }
    }

    return \@neighbors;
}

# counts the number of agent types there is in the grid or subregion
sub _find_agent_count {
    my ($grid, $row1, $col1, $row2, $col2) = @_;
    my $rowcount = 0;
    my %count;

    for (@$grid) {
        # no skip if rows, cols not defined
        next if defined $row1 && defined $row2
            && ($rowcount < $row1 || $rowcount > $row2);

        my $colcount = 0;
        for my $agent (@$_) {
            $count{$agent}++ if (!defined $col1 && !defined $col2) # ditto
                || ($colcount >= $col1 && $colcount <= $col2);
            $colcount++;
        }

        $rowcount++;
    }

    return \%count;
}

1;
