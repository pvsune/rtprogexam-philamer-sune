use strict;
use warnings;

use Test::More;
require Schelling;

my $gridfile = 'grid.txt';
my $grid = Schelling::loadgrid($gridfile);

is_deeply( Schelling::_find_neighbors($grid, 0, 0), [qw/X X O/] );
is_deeply( Schelling::_find_neighbors($grid, 9, 8), [qw/O X O X X/] );

my $neighbors = Schelling::_find_neighbors($grid, 3, 4);
is( Schelling::_is_agent_satisfied($neighbors, 'X', 0.5), 1 );

$neighbors = Schelling::_find_neighbors($grid, 5, 6);
isnt( Schelling::_is_agent_satisfied($neighbors, 'O', 0.5), 1 );

is( Schelling::schelling($grid, 0), -1 );
is( Schelling::schelling($grid, 1), -1 );
is( Schelling::schelling($grid, -1), -1 );

$grid = Schelling::loadgrid('simple_grid.txt');
is_deeply( Schelling::schelling($grid, 0.33),
    [ ['O', 'O', ' '], [' ', 'O', 'O'], ['X', ' ', 'O'] ] );

done_testing;
