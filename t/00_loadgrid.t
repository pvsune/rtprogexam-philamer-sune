use strict;
use warnings;

use Test::More;
require Schelling;

my $gridfile = 'grid.txt';
my $grid = Schelling::loadgrid($gridfile);

is( _slurp($gridfile), _grid_to_str($grid) );
is( Schelling::loadgrid('invalid_grid.txt'), -1 );
is_deeply ( Schelling::_find_grid_dimensions($grid), [10, 10] );

sub _grid_to_str {
    my $grid = shift;

    my @line;
    push @line, join '', @$_ foreach @$grid;

    return join "\n", @line;
}

sub _slurp {
    my $file;
    { local $/ = undef;
        local *FILE;
        open FILE, shift;
        $file = <FILE>;
        close FILE }

    return $file;
}

done_testing;
