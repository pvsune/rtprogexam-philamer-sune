use strict;
use warnings;

use Test::More;
require Schelling;

my $gridfile = 'grid.txt';
my $grid = Schelling::loadgrid($gridfile);

is( Schelling::dissimilarity($grid, 0, 6, 3, 9), 0.02091 );
is( Schelling::dissimilarity($grid, 4, 4, 2, 2), -1 );

# WANT: check how the script would fair given a very big grid file

done_testing;
